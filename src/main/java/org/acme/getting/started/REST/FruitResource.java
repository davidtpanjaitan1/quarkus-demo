package org.acme.getting.started.REST;

import org.jboss.logging.Logger;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.*;

@Path("/fruits")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class FruitResource {
    private static final Logger LOG = Logger.getLogger(FruitResource.class);

    private Set<Fruit> fruits = Collections.newSetFromMap(Collections.synchronizedMap(new LinkedHashMap<>()));

    public FruitResource() {
        fruits.add(new Fruit("Apple", "Winter fruit"));
        fruits.add(new Fruit("Pineapple", "Tropical fruit"));
    }

    @GET
    public Set<Fruit> list() {
        LOG.info("Get All Fruit");
        return fruits;
    }

    @GET
    @Path("/{name}")
    public Fruit getOne(@PathParam("name") String name) {
        return fruits.stream().filter(s -> s.name.equals(name)).findFirst().get();
    }

    // Request body bisa pake hashmap atau bisa lgsg objectnya
//    @POST
//    public HashMap<String, String> coba(HashMap<String, String> values) {
//        return values;
//    }

    @POST
    public Set<Fruit> add(@Valid Fruit fruit) {
        fruits.add(fruit);
        return fruits;
    }

    @DELETE
    public Set<Fruit> delete(Fruit fruit) {
        fruits.removeIf(existingFruit -> existingFruit.name.contentEquals(fruit.name));
        return fruits;
    }
}
