package org.acme.getting.started;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/hello")
public class GreetingResource {

    @GET
    @Path("/{id}")
    public String hello(@PathParam("id") int id, @QueryParam("param") String param) {
        return "hello " + id + " " + param;
    }

    @GET
    @Path("/ok")
    public Response getOkResponse() {

        String message = "This is a text response";

        return Response
                .status(Response.Status.OK)
                .entity(message)
                .build();
    }
}